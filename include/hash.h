#ifndef HASH_H_
#define HASH_H_

#define HASH_A 31
#define HASH_B 971
#define ROUNDS 4
#include <string>
#include <iostream>
namespace hash {
    // Контрольная сумма строки   
    size_t hash(std::string);
}

#endif //HASH_H_
