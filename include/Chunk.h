#ifndef CHUNK_H_
#define CHUNK_H_

#include <algorithm>
#include <string>
#include <iostream>

#define CHUNK_SIZE 64
#define DATA_CHUNK_CAPACITY (CHUNK_SIZE - sizeof(size_t))
#define CHUNK_HEAD_FLAG 1
#define CHUNK_DATA_FLAG 2

#define FLAG_POS 0
//позиции байтов, начиная с которых будут храниться данные числа
//число имеет тип size_t и занимает 8 байт
#define COUNT_DATA_CHUNKS_POS  (1 * sizeof(size_t))
#define COUNT_DATA_BYTES_POS   (2 * sizeof(size_t))
#define HASH_POS               (3 * sizeof(size_t))
#define FILENAME_SIZE_POS      (4 * sizeof(size_t))

#define LEFT_DATA_POS (sizeof(size_t))

/*
    кусок данных состоящий CHUNK_SIZE байт и которые хранят информацию
    первые sizeof(size_t) байт хранят флаги типа чанка, 
   */

class Chunk { // кусок данных, который содержит передаваемую информацию и имеет фиксированный размер
 public:
     Chunk(){};
     std::string get_raw_data() const {return data;};
     Chunk(char *);
     bool is_head() const;
     virtual std::string get_data() const {return std::string();};
 protected:
     std::string get_data_in_range(size_t, size_t) const;
     std::string data;
     void insert_value_to_pos(size_t, size_t);
     size_t get_value_from_pos(size_t) const;
};


class Head_chunk : public Chunk { // чанк, который хранит информацию о передаваемом файле и служебную информацию 
 public:
    Head_chunk(const Chunk& c) {data = c.get_raw_data();};
    Head_chunk(size_t, size_t, size_t, size_t);
    std::string get_data() const;
    size_t get_count_data_chunks() const;
    size_t get_count_data_bytes() const;
    size_t get_hash() const ;
    size_t get_filename_size() const;
};

class Data_chunk : public Chunk { // чанк, который содержит часть данных файла
 public:
     Data_chunk(const Chunk& c) {data = c.get_raw_data();};
     std::string get_data() const;
     std::string get_data(size_t count) const;
     Data_chunk(const std::string &);
     Data_chunk(const std::string &, size_t);
};

#endif //CHUNK_H_
