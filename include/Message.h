#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "hash.h"
#include "Chunk.h"
/*
    Класс, который разбивает исходный файл на равные куски данных и метаинформацию для удобной передачи по сети
    первый чанк всегда хранит заголовки(метаданные)
    второй чанк хранит название файла
    все остальные чанки хранят содержимое файла
   */


class Message {
 public:
    Message(std::string); //message from filename
    Message(const std::vector<Chunk> &);
    bool is_valid() const;

    std::vector<Chunk> get_chunks() const;

    void save_to_file() const;
 private:
    int data_to_chunks();
    int chunks_to_data();
    std::vector<Chunk> chunks;
    std::string data;
    std::string filename;
    bool valid;
};

#endif //MESSAGE_H_
