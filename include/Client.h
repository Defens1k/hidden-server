#ifndef CLIENT_H_
#define CLIENT_H_

#include <string.h> 
#include <stdlib.h> 
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h> 


#include "Message.h"
#define PORT 8888
class Client {
 public:
    Client();
    ~Client();
    void main_loop();
 private:
    int read_chunk();
    int s, i, recv_len;
    char buf[CHUNK_SIZE];
    struct sockaddr_in si_me, si_other;
    socklen_t slen;
    std::vector<Chunk> chunks;
};

#endif //CLIENT_H_
