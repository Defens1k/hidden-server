#include <stdio.h>	//printf
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h> 
#include <string>

#include "Client.h" // include PORT defination
#include "Message.h"

void die(std::string s) {
	perror(s.c_str());
	exit(1);
}

int main(void) {
	struct sockaddr_in  si_other;
	
	int s, i , recv_len;
    socklen_t slen = sizeof(si_other);
	char buf[CHUNK_SIZE];
	// Используем протокол UDP , ведь для него не нужна обратная связь с скрытым сервером
	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		die("socket");
	}
	memset((char *) &si_other, 0, sizeof(si_other));
	
    std::cout << "input client ip addr:\n";
    std::string ip;
    std::cin >> ip;	

    si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	si_other.sin_addr.s_addr = htonl(inet_network(ip.c_str()));
	
	
    std::cout << "Ведите название файла\n\n";
    
    std::string filename;
    std::cin >> filename;
    Message msg(filename.c_str());
    std::vector<Chunk> chunks = msg.get_chunks();
    for (std::vector<Chunk>::iterator i = chunks.begin(); i != chunks.end(); i++) {
        if (sendto(s, (*i).get_raw_data().c_str(), CHUNK_SIZE, 0, (struct sockaddr*) &si_other, slen) == -1) { //Посылаем данные по частям(чанкам)
            die("sendto()");
        }
    }
	close(s);
	return 0;
}
