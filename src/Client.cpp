#include "Client.h"



Client::Client() {
	
    slen = sizeof(si_other);
	
	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        std::cerr << "socket error\n";
	}
	
	memset((char *) &si_me, 0, sizeof(si_me));
	
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	
	//bind socket to port
	if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1) {
        std::cerr << "bind error\n";
	}
}

int Client::read_chunk() {
    return recvfrom(s, buf, CHUNK_SIZE, 0, (struct sockaddr *) &si_other, &slen);
}


void Client::main_loop() {
	while(1) {
        chunks.clear();
        std::cout << "Waiting for data...\n";
		if ((recv_len = read_chunk()) == -1) {
            std::cerr << "recvfrom\n";
		}
	    Head_chunk first = Chunk(buf); // читаем первый заголовочный чанк
        if (first.is_head() == true) {
            chunks.push_back(first);
            for (size_t i = 0; i < first.get_count_data_chunks() + 1; i++) {
                if ((recv_len = read_chunk()) == -1) {
                    std::cerr << "recvfrom\n";
                }
                chunks.push_back(Chunk(buf));
            }
        } else {
            continue;
        }
        Message msg(chunks);
        if (msg.is_valid()) {
            msg.save_to_file();
            std::cout << "Valid! save to file!";
        } else {
            std::cout << "Invalid message, file error\n";
        }
    }
}

Client::~Client() {
    close(s);
}
