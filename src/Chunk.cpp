#include "Chunk.h"

Chunk::Chunk(char * bytes) {
    for (size_t i = 0; i < CHUNK_SIZE; i++) {
        data.push_back(bytes[i]);
    }
}

bool Chunk::is_head() const {
    if (get_value_from_pos(FLAG_POS) == CHUNK_HEAD_FLAG) {
        return true;
    } else {
        return false;
    }
}

std::string Chunk::get_data_in_range(size_t left, size_t right) const{
    std::string result;
    for (size_t i = left; i < std::min(static_cast<size_t>(CHUNK_SIZE), right); i++) {
        result.push_back(data[i]);
    }
    return result;
}

void Chunk::insert_value_to_pos(size_t value, size_t pos) {
    for (size_t i = 0; i < sizeof(value); i++) { // insert byte to byte
        size_t sdvig = sizeof(value) * (sizeof(value) - 1 - i);
        size_t inserting_byte = value >> sdvig;
        data[pos + i] = inserting_byte & 0xFF;
    }
}

size_t Chunk::get_value_from_pos(size_t pos) const {
    size_t result = 0;
    for (size_t i = 0; i < sizeof(result); i++) {
        size_t sdvig = sizeof(result) * (sizeof(result) - 1 - i);
        unsigned char byte = data[pos + i];
        result += (size_t)byte << sdvig;
    }
    return result;
}
 
Head_chunk::Head_chunk(size_t count_data_chunks, size_t count_bytes, size_t hash, size_t filename_size) {
    for (size_t i = 0; i < CHUNK_SIZE; i++) {
        data.push_back(' ');  // insert random symbols
    }
    insert_value_to_pos(CHUNK_HEAD_FLAG  , FLAG_POS);
    insert_value_to_pos(count_data_chunks, COUNT_DATA_CHUNKS_POS);
    insert_value_to_pos(count_bytes      , COUNT_DATA_BYTES_POS);
    insert_value_to_pos(hash             , HASH_POS);
    insert_value_to_pos(filename_size    , FILENAME_SIZE_POS);
}

std::string Head_chunk::get_data() const {
    return std::string();
}

size_t Head_chunk::get_count_data_chunks() const {
    return get_value_from_pos(COUNT_DATA_CHUNKS_POS);
}

size_t Head_chunk::get_count_data_bytes() const {
    return get_value_from_pos(COUNT_DATA_BYTES_POS);
}

size_t Head_chunk::get_hash() const {
    return get_value_from_pos(HASH_POS);
}

size_t Head_chunk::get_filename_size() const {
    return get_value_from_pos(FILENAME_SIZE_POS);
}

Data_chunk::Data_chunk(const std::string & input) {
    for (size_t i = 0; i < CHUNK_SIZE; i++) {
        data.push_back(' '); // заполняем любыми символами
    }
    insert_value_to_pos(CHUNK_DATA_FLAG, FLAG_POS);
    for (size_t i = LEFT_DATA_POS; i < input.size() + LEFT_DATA_POS; i++) {
        data[i] = input[i - LEFT_DATA_POS]; // перезаписываем данными
    }
}

Data_chunk::Data_chunk(const std::string & input, size_t pos) {
    for (size_t i = 0; i < CHUNK_SIZE; i++) {
        data.push_back(' ');
    }
    insert_value_to_pos(CHUNK_DATA_FLAG, FLAG_POS);   
    if (pos > input.size()) {
        return;
    }
    for(size_t i = 0; i < DATA_CHUNK_CAPACITY; i++) {
        data[i + LEFT_DATA_POS] = input[pos + i];
    }
}

std::string Data_chunk::get_data() const {
    return get_data_in_range(LEFT_DATA_POS, LEFT_DATA_POS + DATA_CHUNK_CAPACITY);
}

std::string Data_chunk::get_data(size_t count) const{
    if (count > DATA_CHUNK_CAPACITY) {
        count = DATA_CHUNK_CAPACITY;
    }
    return get_data_in_range(LEFT_DATA_POS, LEFT_DATA_POS + count);
}





