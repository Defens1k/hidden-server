#include "hash.h"
size_t hash::hash(std::string str) {
    size_t hash = 0;
    for (std::string::iterator i = str.begin(); i != str.end(); i++) {
        for (size_t round = 0; round != ROUNDS; round++) {
            hash += *i + HASH_A;
            hash *= HASH_B;
            hash += HASH_A;
        }
    }
    return hash;
}

