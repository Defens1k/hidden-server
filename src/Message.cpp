#include "Message.h"

Message::Message(std::string input_filename) {
    std::ifstream f(input_filename.c_str());
    char c = 0;
    while ((c = f.get()) != EOF) {
        data.push_back(c);
    }
    filename = input_filename;
    data_to_chunks(); // разбиваем данные на части
    valid = true;
}

Message::Message(const std::vector<Chunk> & input_chunks) {
    chunks = input_chunks;
    valid = true;
    chunks_to_data(); 
}

bool Message::is_valid() const {
    return valid;
}

std::vector<Chunk> Message::get_chunks() const {
    return chunks;
}

void Message::save_to_file() const {
    std::ofstream file(filename.c_str());
    file << data;
}

int Message::data_to_chunks() {
    chunks.clear();
    size_t count_data_chunks = 1 + (data.size() - 1)/ DATA_CHUNK_CAPACITY;
    Chunk first = Head_chunk(count_data_chunks, data.size(), hash::hash(std::string(filename + data)), filename.size());
    chunks.push_back(first);

    Chunk filename_chunk = Data_chunk(filename);
    chunks.push_back(filename_chunk);
    for (size_t i = 0; i < data.size(); i += DATA_CHUNK_CAPACITY) {
        chunks.push_back(Data_chunk(data, i));
    }
    return 0;
}

int Message::chunks_to_data() {
    if (chunks.size() < 3) {
        std::cerr << "to small chunks read\n";
        valid = false;
        return -1;
    }
    // берем основную информацию из первого чанка
    if (chunks[0].is_head() == false) {
        std::cerr << "not head chunks first\n";
        valid = false;
        return -2;
    }
    Head_chunk headers = static_cast<Head_chunk>(chunks[0]);
    size_t count_data_chunks = headers.get_count_data_chunks();
    if (count_data_chunks + 2 != chunks.size()) {
        std::cerr << "invalid count of chunks\n";
        valid = false;
        return -3;
    }
    size_t count_bytes = headers.get_count_data_bytes();
    size_t hash = headers.get_hash();
    size_t filename_size = headers.get_filename_size();
    filename = (static_cast<Data_chunk>(chunks[1]).get_data(filename_size));
    filename.resize(filename_size); // считываем название файла и обрезаем по длинне
    data.clear();
    std::vector<Chunk>::iterator i = chunks.begin();
    i++;   // skip head chunk
    i++;   // skip filename chunk
    while (i != chunks.end()) {
        data = data + static_cast<Data_chunk>(*i).get_data();
        i++;
    }
    data.resize(count_bytes);
    if (hash != hash::hash(std::string(filename + data))) { // проверяем конторльную сумму данных
        std::cerr << "hash not valid\n";
        valid = false;
        return -4;
    }
    return 0;
}










